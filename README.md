# Facets Date Range Picker

The Facets Date Range Picker provides a widget for facets to pick a date range.
The following ranges are supported:

- Today
- Tomorrow
- This weekend (Sat to Sun)
- This weekend (Fri to Sun)
- Next 7 days
- Next 14 days
- Next 30 days

**Datepicker**

A datepicker facet widget is also available.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/facets_date_range_picker).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/facets_date_range_picker).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

The module is tested with Search API and Search API Solr.

- [Search API](https://www.drupal.org/project/search_api)
- ([Search API Solr](https://www.drupal.org/project/search_api_solr))
- [Facets](https://www.drupal.org/project/facets)

This processor and query type expects the selected field to be indexed as
timestamp or this module will not work.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Add the date range field values to the search index
    1. The start date range as a string (timestamp)
    2. The end date range as a string (timestamp)
2. Add a facet and enable the Date Range Picker
   1. Select the start date field
   2. Select the end date field
   3. Choose the options you would like to have
        1. Overlap (This lets you have items starting before the start date and
           stopping after the end date.)
        2. Date range picker options
3. Optionally add a reset link


## Maintainers

- [Tim Diels (tim-diels)](https://www.drupal.org/u/tim-diels)

**This project has been sponsored by:**
- [Calibrate](https://www.calibrate.be)
